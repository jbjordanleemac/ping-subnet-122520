#!/bin/bash

echo -n "Please enter the subnet you wish to ping thru. Enter the first 3 octets only. ex. 192.168.1: "
read subnet

pingcmd=/usr/bin/ping
hostcmd=/usr/bin/host
number=1

while [ $number -lt 255 ]
do
  ipaddr=$subnet.$number
  number=$(( number + 1 ))
  $pingcmd $ipaddr -c 1 -w 1 >/dev/null 
  if [ $? -eq 0 ]
  then
    echo "ip addr $ipaddr is being used && hostname is `$hostcmd $ipaddr`"
  fi  
done 
